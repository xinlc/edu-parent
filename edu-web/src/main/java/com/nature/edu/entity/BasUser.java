package com.nature.edu.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 *
 * @author wangck
 * @since 2019-08-07
 */
@TableName("bas_user")
public class BasUser implements Serializable {


    /**
     * 用户Id，主键
     */
    @TableId("user_id")
    private String userId;
    /**
     * 账号
     */
    private String userNo;
    /**
     * 登录名
     */
    private String userName;
    /**
     * 头像
     */
    private String userHead;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 姓名
     */
    private String personName;
    /**
     * 姓名全拼音
     */
    private String pinyName;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserHead() {
        return userHead;
    }

    public void setUserHead(String userHead) {
        this.userHead = userHead;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPinyName() {
        return pinyName;
    }

    public void setPinyName(String pinyName) {
        this.pinyName = pinyName;
    }

    @Override
    public String toString() {
        return "BasUser{" +
        "userId=" + userId +
        ", userNo=" + userNo +
        ", userName=" + userName +
        ", userHead=" + userHead +
        ", nickName=" + nickName +
        ", personName=" + personName +
        ", pinyName=" + pinyName +
        "}";
    }
}
